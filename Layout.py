import sys
from PyQt5 import QtWidgets as qw
from PyQt5 import QtGui as qg
from PyQt5 import QtCore as qc


class Window(qw.QMainWindow):
    def __init__(self, parent=None):
        super(qw.QMainWindow, self).__init__(parent)

        self.init_window()
        self.form = FormWidget()
        self.setCentralWidget(self.form)

        self.show()

    def init_window(self):
        self.height = 400
        self.width = 400
        self.setWindowTitle("3D Viewer")
        self.setFixedSize(self.width, self.height)


class FormWidget(qw.QWidget):
    def __init__(self, parent=None):
        super(qw.QWidget, self).__init__(parent)

        self.height = 400
        self.width = 400

        self.init_canvas()

        hbox = qw.QVBoxLayout()
        hbox.addWidget(self.display)
        hbox.addWidget(self.btn)

        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.setSpacing(0)

        self.setLayout(hbox)

    def init_canvas(self):
        self.display = qw.QLabel()
        # self.setCentralWidget(self.display)

        self.canvas = qg.QPixmap(self.width, self.height // 2)
        self.painter = qg.QPainter(self.canvas)
        self.painter.fillRect(0, 0, self.width, self.height, qc.Qt.black)

        self.display.setPixmap(self.canvas)

        # 2

        #self.display2 = qw.QLabel()
        #self.canvas2 = qg.QPixmap(self.width, self.height // 2)
        #self.painter.fillRect(0, 0, self.width, self.height, qc.Qt.green)

        #self.display2.setPixmap(self.canvas)

        self.btn = qw.QPushButton("TEst")


        self.painter.end()


# Start app
if __name__ == '__main__':
    app = qw.QApplication(sys.argv)
    View = Window()
    sys.exit(app.exec_())
