from PyQt5 import QtWidgets as qw
from PyQt5 import QtCore as qc
from PyQt5 import QtGui as qg


class FormWidget(qw.QWidget):

    def __init__(self, parent):
        super(FormWidget, self).__init__(parent)

        self.height = 500
        self.width = 500

        self.create_horizontal_layout1()
        self.create_horizontal_layout2()
        self.create_horizontal_layout3()
        self.create_horizontal_layout4()
        self.create_viewing_label()

        window_layout = qw.QVBoxLayout()
        window_layout.addWidget(self.display)
        window_layout.addWidget(self.horizontalGroupBox3)
        window_layout.addWidget(self.horizontalGroupBox1)
        window_layout.addWidget(self.horizontalGroupBox2)
        window_layout.addWidget(self.horizontalGroupBox4)

        self.setLayout(window_layout)

    def create_horizontal_layout1(self):
        self.horizontalGroupBox1 = qw.QGroupBox("Polygon Colors")
        layout = qw.QHBoxLayout()

        # --------------- Buttons-----------------#
        self.button_blue = qw.QPushButton('Blue', self)
        layout.addWidget(self.button_blue)

        self.button_red = qw.QPushButton('Red', self)
        layout.addWidget(self.button_red)

        self.button_green = qw.QPushButton('Green', self)
        layout.addWidget(self.button_green)

        self.button_random = qw.QPushButton('Random', self)
        layout.addWidget(self.button_random)

        # --------------- Set Layout --------------#
        self.horizontalGroupBox1.setLayout(layout)

    def create_horizontal_layout2(self):
        self.horizontalGroupBox2 = qw.QGroupBox("Object Movement")
        layout = qw.QHBoxLayout()

        # --------------- Buttons-----------------#
        self.button_left = qw.QPushButton('Left', self)
        layout.addWidget(self.button_left)

        self.button_right = qw.QPushButton('Right', self)
        layout.addWidget(self.button_right)

        self.button_up = qw.QPushButton('Up', self)
        layout.addWidget(self.button_up)

        self.button_down = qw.QPushButton('Down', self)
        layout.addWidget(self.button_down)

        self.button_forward = qw.QPushButton('Forward', self)
        layout.addWidget(self.button_forward)

        self.button_backward = qw.QPushButton('Backward', self)
        layout.addWidget(self.button_backward)

        # --------------- Set Layout --------------#
        self.horizontalGroupBox2.setLayout(layout)

    def create_horizontal_layout3(self):
        self.horizontalGroupBox3 = qw.QGroupBox("Object Selection")
        layout = qw.QHBoxLayout()

        # --------------- Buttons-----------------#
        self.button_cube = qw.QPushButton('Cube', self)
        layout.addWidget(self.button_cube)

        self.button_bun = qw.QPushButton('Bunny', self)
        layout.addWidget(self.button_bun)

        self.button_dragon = qw.QPushButton('Dragon', self)
        layout.addWidget(self.button_dragon)

        # --------------- Set Layout --------------#
        self.horizontalGroupBox3.setLayout(layout)

    def create_horizontal_layout4(self):
        self.horizontalGroupBox4 = qw.QGroupBox("Rotation Axis")
        layout = qw.QHBoxLayout()

        # --------------- Buttons-----------------#
        self.checkbox_x = qw.QRadioButton('X Axis', self)
        layout.addWidget(self.checkbox_x)

        self.checkbox_y = qw.QRadioButton('Y Axis', self)
        self.checkbox_y.setChecked(True)
        layout.addWidget(self.checkbox_y)

        self.checkbox_z = qw.QRadioButton('Z Axis', self)
        layout.addWidget(self.checkbox_z)

        # --------------- Set Layout --------------#
        self.horizontalGroupBox4.setLayout(layout)

    def create_viewing_label(self):
        self.display = qw.QLabel()
        self.canvas = qg.QPixmap(self.width, self.height)
        self.painter = qg.QPainter(self.canvas)
        self.painter.fillRect(0, 0, self.width, self.height, qc.Qt.white)
        self.painter.end()
        self.display.setPixmap(self.canvas)
