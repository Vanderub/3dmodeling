import sys
from PyQt5 import QtWidgets as qw
from PyQt5 import QtGui as qg
from PyQt5 import QtCore as qc
from Polygons import Polygon
from FormWidget import FormWidget
import numpy as np
import csv


# ---------------------------------------------------- Main Window Class ----------------------------------------------#
class Viewer(qw.QMainWindow):

    def __init__(self, parent=None):
        super(Viewer, self).__init__(parent)

        self.init_window()

        # init bunny ---------
        self.polylist = "cube.csv"
        self.polygon_object = self.init_poly(self.polylist, 10)  # 10 for scaling

        # cam -----------
        self.init_cam(self.polygon_object, 3)  # number of polygon points

        # controls
        self.init_buttons()

        # Polygon Color
        self.polygon_color = qc.Qt.gray

        # Rotation axis
        self.axis = "y"

        # draw 3d obj
        # self.draw_polygon(2, self.sort_polygon(cube_obj), 4)
        self.draw_polygon(2, self.polygon_object, 3)

        self.show()

    def init_window(self):
        self.setWindowTitle("3D Viewer")

        self.setStyleSheet('QMainWindow{background-color: darkgray}')

        # Setting the Layout of the main UI
        self.form_widget = FormWidget(self)
        self.setCentralWidget(self.form_widget)

    def set_polygon_cube(self):
        self.polylist = "cube.csv"
        self.polygon_object = self.init_poly(self.polylist, 10)
        self.init_cam(self.polygon_object, 3)
        self.draw_polygon(2, self.polygon_object, 3)

    def set_polygon_bunny(self):
        self.polylist = "bun_zipper_res4.csv"
        self.polygon_object = self.init_poly(self.polylist, 10)  # 10 for scaling
        self.init_cam(self.polygon_object, 3)
        self.draw_polygon(2, self.polygon_object, 3)

    def set_polygon_dragon(self):
        self.polylist = "dragon_vrip_res4.csv"
        self.polygon_object = self.init_poly(self.polylist, 18)  # 10 for scaling
        self.init_cam(self.polygon_object, 3)
        self.draw_polygon(2, self.polygon_object, 3)

    def init_buttons(self):
        # Movement
        self.form_widget.button_up.clicked.connect(self.cam.down)
        self.form_widget.button_down.clicked.connect(self.cam.up)
        self.form_widget.button_left.clicked.connect(self.cam.left)
        self.form_widget.button_right.clicked.connect(self.cam.right)
        self.form_widget.button_forward.clicked.connect(self.cam.forward)
        self.form_widget.button_backward.clicked.connect(self.cam.backward)

        # Colors
        self.form_widget.button_blue.clicked.connect(self.set_color_blue)
        self.form_widget.button_red.clicked.connect(self.set_color_red)
        self.form_widget.button_green.clicked.connect(self.set_color_green)
        self.form_widget.button_random.clicked.connect(self.rnd_color)

        #polygons
        self.form_widget.button_cube.clicked.connect(self.set_polygon_cube)
        self.form_widget.button_bun.clicked.connect(self.set_polygon_bunny)
        self.form_widget.button_dragon.clicked.connect(self.set_polygon_dragon)

    def init_cam(self, polygon, n):
        self.cam = Cam(polygon, n, (0, 0, -5))
        self.keyPressEvent = self.cam.update

    def rnd_color(self):
        color = list(np.random.choice(range(256), size=3))
        c_color = qg.QColor(color[0], color[1], color[2])
        self.polygon_color = c_color
        self.draw_polygon(2, View.polygon_object, 3)

    def set_color_blue(self):
        self.polygon_color = qc.Qt.blue
        self.draw_polygon(2, View.polygon_object, 3)

    def set_color_yellow(self):
        self.polygon_color = qc.Qt.yellow
        self.draw_polygon(2, View.polygon_object, 3)

    def set_color_red(self):
        self.polygon_color = qc.Qt.red
        self.draw_polygon(2, View.polygon_object, 3)

    def set_color_green(self):
        self.polygon_color = qc.Qt.green
        self.draw_polygon(2, View.polygon_object, 3)

    def draw_polygon(self, projection, polygon, n):
        """
        1) Man nehme die Anfangs-Koordianten des jew. Polygonobjektes und rechne die koordinaten nach jew. Perspektive um
        2) Für die neuen Koordianten brauchen wir ein "Behältnis" da Tuple immutable sind
        3) Zeichnen der Punkte und Verbindungsstücke
        :param projection: parallel, cav, central
        """
        # ------------------------------------- Umrechen + zeichnen neuer Koordinaten ---------------------------------#

        self.painter = qg.QPainter(self.form_widget.canvas)
        self.painter.fillRect(0, 0, self.form_widget.width, self.form_widget.height, qc.Qt.green)

        # Middlepoint
        cx = self.form_widget.width // 2
        cy = self.form_widget.height // 2

        new_poly_array = []
        # ---------------------------------------------- parallel -----------------------------------------------------#

        if projection == 0:  # parallel
            for poly in polygon.polygons:
                for trip in poly:
                    x = trip[0] + trip[2]
                    y = trip[1] + trip[2]
                    self.painter.drawPoint(x, y)
                    new_poly_array.append((x, y))

        # ---------------------------------------------- cavalier -----------------------------------------------------#
        if projection == 1:  # cav
            for poly in polygon.polygons:
                for trip in poly:
                    x = trip[0] + 0.5 * np.sqrt(2) * trip[2]
                    y = trip[1] + 0.5 * np.sqrt(2) * trip[2]
                    self.painter.drawPoint(x, y)
                    new_poly_array.append((x, y))

        # ---------------------------------------------- central  -----------------------------------------------------#
        if projection == 2:  # central projection
            for poly in polygon.polygons:
                for trip in poly:
                    x, y, z = trip[0], trip[1], trip[2]
                    point = np.array([x, y, z])

                    # 1 rotation
                    if self.form_widget.checkbox_x.isChecked():
                        rotated_point = polygon.rotate_point(point, self.cam.deg, "x")
                    elif self.form_widget.checkbox_y.isChecked():
                        rotated_point = polygon.rotate_point(point, self.cam.deg, "y")
                    elif self.form_widget.checkbox_z.isChecked():
                        rotated_point = polygon.rotate_point(point, self.cam.deg, "z")

                    # 2 movement of camera
                    rotated_point = np.add(rotated_point, self.cam.pos)

                    # 3 central perpective
                    # print("z: ", rotated_point[2])

                    # Zentralprojektion invertiert (hinten größer, vorne kleiner)?
                    # f = 400 / rotated_point[2] - self.cam.pos[2]

                    # Zentralprojektion ohne Auswirkung?
                    f = 400 / self.cam.pos[2] - rotated_point[2]

                    # Ohne Zentralprojektion
                    # f = -100
                    # print("f: ", f)

                    x, y = rotated_point[0] * f, rotated_point[1] * f

                    # self.painter.drawPoint(x + cx, y + cy)
                    if self.polylist == "dragon_vrip_res4.csv":
                        new_poly_array.append((x + cx, y + cy + 150, rotated_point[2]))
                    elif self.polylist == "bun_zipper_res4.csv":
                        new_poly_array.append((x + cx, y + cy + 80, rotated_point[2]))
                    else:
                        new_poly_array.append((x + cx, y + cy, rotated_point[2]))
        obj = Polygon(n, *new_poly_array)
        obj = obj.sort_polygon()

        # ---------------------------------- converting coord's to qt.F's for drawing purposes-------------------------#
        # change (x,y) to QPointF
        qpointf_ar = []
        for i in range(len(obj.polygons)):
            tmp = []
            for j in range(len(obj.polygons[0])):
                point = qc.QPointF()
                point.setX(obj.polygons[i][j][0])
                point.setY(obj.polygons[i][j][1])
                tmp.append(point)
            qpointf_ar.append(tmp)

        # make from QpointF's a QpolygonF = consists of 4 QpointF
        polygon_f = []
        for array in qpointf_ar:
            tmp = qg.QPolygonF()
            for item in array:
                tmp.append(item)
            polygon_f.append(tmp)

        # QPainterPath from QPolygonF // each Path is a QPolygonF which consists of 4 QPointF
        # painter_path = []
        i = 0
        j = 0
        for polyf in polygon_f:
            test_path = qg.QPainterPath()
            test_path.addPolygon(polyf)
            brush = qg.QBrush(self.polygon_color, qc.Qt.SolidPattern)
            self.painter.fillPath(test_path, brush)

            # ---------------------------------------------------- drawing lines --------------------------------------#
            if n == 3:
                # draw line for triangle
                self.painter.setPen(qg.QPen(qc.Qt.black, 1, qc.Qt.SolidLine))

                self.painter.drawLine(qpointf_ar[i][j], qpointf_ar[i][j + 1])
                self.painter.drawLine(qpointf_ar[i][j + 1], qpointf_ar[i][j + 2])
                self.painter.drawLine(qpointf_ar[i][j + 2], qpointf_ar[i][j])
                i += 1
            if n == 4:
                # draw line for rect
                self.painter.setPen(qg.QPen(qc.Qt.black, 1, qc.Qt.SolidLine))

                self.painter.drawLine(qpointf_ar[i][j], qpointf_ar[i][j + 1])
                self.painter.drawLine(qpointf_ar[i][j + 1], qpointf_ar[i][j + 2])
                self.painter.drawLine(qpointf_ar[i][j + 2], qpointf_ar[i][j + 3])
                self.painter.drawLine(qpointf_ar[i][j + 3], qpointf_ar[i][j])
                i += 1

        self.painter.end()

        self.form_widget.display.setPixmap(self.form_widget.canvas)

    # ------------------------------------------------ read mesh file -------------------------------------------------#
    def init_poly(self, polylist, scaling):
        with open(polylist) as fr:
            reader = csv.reader(fr, delimiter=" ")
            start_row = 12
            points = []
            connected_points = []
            line_count = 0
            el_vertex = 0
            n = 0
            for row in reader:
                line_count += 1
                # here's the amount of total points
                if line_count == 4:
                    el_vertex = int(row[2])
                # the x,y,z coordinates for each point
                if line_count > start_row and line_count <= start_row + el_vertex:
                    points.append((float(row[0]), float(row[1]), float(row[2])))
                # information: which points form a polygon
                elif line_count > start_row + el_vertex:
                    n = int(row[0])
                    connected_points.append((int(row[1]), int(row[2]), int(row[3])))

        tmp = []
        minimum = min(points, key=lambda x: x[2])
        minimum = abs(minimum[2]) * scaling
        print(minimum)
        for trip in connected_points:
            x = points[trip[0]]
            (x, y, z) = x[0] * scaling, x[1] * scaling, x[2] * scaling
            tmp.append((x, y, z))

            y = points[trip[1]]
            (x, y, z) = y[0] * scaling, y[1] * scaling, y[2] * scaling
            tmp.append((x, y, z))

            z = points[trip[2]]
            (x, y, z) = z[0] * scaling, z[1] * scaling, z[2] * scaling
            tmp.append((x, y, z))

        # make obj
        obj = Polygon(n, *tmp)
        return obj


# ----------------------------------------------------- Camera Class --------------------------------------------------#
class Cam:
    def __init__(self, polygon, n, pos=(0, 0, 0), degree=0):
        self.pos = list(pos)
        self.deg = degree
        self.polygon = polygon
        self.n = n

        # moving var
        self.s = 0.1  # cam move
        self.rad = 0.01  # rotation move

    # ----------------------------------------- CATCHING KEY press events ---------------------------------------------#

    def update(self, event):

        # UP and DOWN
        if event.key() == qc.Qt.Key_E:
            self.up()
        if event.key() == qc.Qt.Key_Q:
            self.down()

        # x, y = s * np.sin(self.rot[0]), s * np.cos(self.rot[1])

        # FRONT ,BACK, LEFT ROTATION and RIGHT ROTATION
        if event.key() == qc.Qt.Key_W:
            self.forward()
        if event.key() == qc.Qt.Key_S:
            self.backward()
        if event.key() == qc.Qt.Key_A:
            self.left()
        if event.key() == qc.Qt.Key_D:
            self.right()

    # ------------------------------------------------ CAM MOVE EVENTS ------------------------------------------------#
    def up(self):
        self.pos[1] -= self.s
        Viewer.draw_polygon(View, 2, View.polygon_object, self.n)

    def down(self):
        self.pos[1] += self.s
        Viewer.draw_polygon(View, 2, View.polygon_object, self.n)

    def forward(self):
        self.pos[2] += self.s
        Viewer.draw_polygon(View, 2, View.polygon_object, self.n)

    def backward(self):
        self.pos[2] -= self.s
        Viewer.draw_polygon(View, 2, View.polygon_object, self.n)

    def left(self):
        self.deg -= (2 * np.pi) / 100
        Viewer.draw_polygon(View, 2, View.polygon_object, self.n)

    def right(self):
        self.deg += (2 * np.pi) / 100
        Viewer.draw_polygon(View, 2, View.polygon_object, self.n)


# -------------------------------------------------- Testing --------------------------------------------------------- #



# Start app
if __name__ == '__main__':
    app = qw.QApplication(sys.argv)
    View = Viewer()
    sys.exit(app.exec_())
