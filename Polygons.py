# -------------------------------------------------------------------------------------------------------------------- #
#                               Praktikum "mathematische Modellierung am Recher 2"                                     #
#                                       Abschnitt 2: 3D-Visualisierung                                                 #
#                                                                                                                      #
#                           Sean Genkel, André Wetzel, Marko Rubinić, Aron Bartmer-Freund                              #
#                                                                                                                      #
# -------------------------------------------------------------------------------------------------------------------- #
from collections import OrderedDict
import numpy as np


class Polygon:
    # n = size of each individual polygon
    # *argv = list of arbitrary length (in this case 3-tuples (x,y,z))
    def __init__(self, n, *argv):
        self.size = 0
        points = []
        # check format, each point consists of (x,y,z)
        for point in argv:
            #if len(point) == 3:
            points.append(point)
            self.size += 1
            #else:
                #raise TypeError("Element " + str(point) + " needs to have 3 coordinates")

        if len(points) < 3:
            raise TypeError("A Polygon consists of at least 3 points")

        # Slice array into smaller arrays of size n (a cube consists of n=6 polygons)
        poly = []
        while len(points) >= n:
            sliced = points[:n]
            poly.append(sliced)
            points = points[n:]

        # e.g.: polygons = [[(a,b,c),(d,e,f),(g,h,i)],[(),(),()],...]
        self.polygons = poly
        self.tmp_polygons = []

    def size(self):
        return self.size

    def sort_polygon(self):
        sum = 0
        index = 0
        sorted_poly = []
        sum_dict = {}

        # get the z sum of each polygon
        for poly in self.polygons:
            sum = 0
            for trip in poly:
                sum += trip[2]
            sum_dict[str(index)] = sum
            index += 1

        # sort dict by value
        sum_dict = OrderedDict(sorted(sum_dict.items(), key=lambda x: x[1], reverse=True))

        # generate new sorted array
        for key in sum_dict:
            for trip in self.polygons[int(key)]:
                sorted_poly.append(trip)

        # Make array to polygon obj
        sorted_poly_obj = Polygon(3, *sorted_poly)

        return sorted_poly_obj

    @staticmethod
    def rot_x_matrix(deg):
        Rx = np.zeros(shape=(3, 3))
        Rx[0][0] = 1
        Rx[1][1] = np.cos(deg)
        Rx[1][2] = -(np.sin(deg))
        Rx[2][1] = np.sin(deg)
        Rx[2][2] = np.cos(deg)
        return Rx

    @staticmethod
    def rot_y_matrix(deg):
        Ry = np.zeros(shape=(3, 3))
        Ry[0][0] = np.cos(deg)
        Ry[0][2] = np.sin(deg)
        Ry[1][1] = 1
        Ry[2][0] = -(np.sin(deg))
        Ry[2][2] = np.cos(deg)
        return Ry

    @staticmethod
    def rot_z_matrix(deg):
        Rz = np.zeros(shape=(3, 3))
        Rz[0][0] = np.cos(deg)
        Rz[0][1] = -(np.sin(deg))
        Rz[1][0] = np.sin(deg)
        Rz[1][1] = np.cos(deg)
        Rz[2][2] = 1
        return Rz

    def rotate_point(self, point, deg, axis):
        v_rot = []
        if axis == "x":
            v_rot = np.matmul(self.rot_x_matrix(deg), point)
        if axis == "y":
            v_rot = np.matmul(self.rot_y_matrix(deg), point)
        if axis == "z":
            v_rot = np.matmul(self.rot_z_matrix(deg), point)

        return v_rot
